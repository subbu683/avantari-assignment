$(function()
{
    var socket = io.connect('http://localhost:3000');
    var points =[];
    var ctx = $("#line-chart");
    var myChart =new Chart(ctx, {
        type: 'line',
        data: {
            labels: points,
            datasets: [{
                data: [],
                label: "IoT",
                borderColor: "#3e95cd",
                fill: false
            }
        ]
    },
    options: {
        title: {
            display: true,

        }
    }
});
socket.on('result',function(data){
    myChart.data.datasets[0].data.push(data.number);
    var dataLength = myChart.data.datasets[0].data.length
    console.log(dataLength);
    points[dataLength - 1] = dataLength; //This populates the array.  +1 is necessary because arrays are 0 index based and you want to store 1-100 in it, NOT 0-99.
    myChart.update();
});
});
  