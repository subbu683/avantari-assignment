$(function()
{
    $start = $("a#start");
    $stop = $("a#stop");
    $putNumber = $("h2#random");
    $result = $("a#client");
    var socket = io.connect('http://localhost:3000');
    function randomNumber() {
        return Math.floor((Math.random() * 100)+1);
    }
    function setNumber(val)
    {
        $putNumber.html(val);
    }
    socket.on('connect', function(data)
    {
        $start.click(function(e)
        {
            e.preventDefault();
            $start.attr("disabled","disabled");
            id =setInterval(function(){
                var random = randomNumber();
                setNumber(random);
                socket.emit('number', random);
            },5000);
        })
        $stop.click(function(e){
            e.preventDefault();
            socket.emit('end');
            $start.removeAttr("disabled","disabled");
            $stop.attr("disabled","disabled");
            clearInterval(id);
            $putNumber.html("None");

        })
    });

    socket.on('result',function(data){
        $result.click(function(e){
        e.preventDefault();
            console.log(data);
        });
    });
});