var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


var app = express();
var server = app.listen(3000);
var io = require('socket.io').listen(server);

// Connect to AWS dynamodb
var AWS = require('aws-sdk');

AWS.config.update({
  region: "us-east-2",
  endpoint: "dynamodb.us-east-2.amazonaws.com"
});

var docClient = new AWS.DynamoDB.DocumentClient();

var table = "iot-app";


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

console.log('Express listening to the port 3000');

io.on('connection' ,function(socket)
{
  console.log('Socket created');
  
  socket.on('number',function(data){
    var params = {
      TableName : table,
      Item : {
        "time": Date.now(),
        "number": data
      }
    };
    
    docClient.put(params,function(err,data){
      if(err)
      {
        console.log("unable to add item. Error JSON:", JSON.stringify(err,null,2));
      }
      else{
        console.log(data);
      }
    });
  })
});

io.on('connection' ,function(socket)
{


var params = {
  TableName : "iot-app",
}
docClient.scan(params, onScan);
function onScan(err,data)
{
  if(err)
  {
    console.log("Scanning failed :",JSON.stringify(err,null,2));
  }
  else{
    data.Items.forEach(el => {
      socket.emit("result", {'number': el.number})
    });
  }
}


});

module.exports = app;
